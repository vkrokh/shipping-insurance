<?php

class Itransition_ShippingInsurance_Block_CheckoutInsurance extends Mage_Checkout_Block_Onepage_Abstract
{
    public function isFeatureEnabled()
    {
        $helper = Mage::helper('itransition_shippinginsurance');
        return $helper->isFeatureEnabled();
    }

    public function listInsuranceCosts()
    {
        $quote = $this->getQuote();
        $helper = Mage::helper('itransition_shippinginsurance');
        $shippingAddress = $quote->getShippingAddress();
        $rates = $shippingAddress->collectShippingRates()->getGroupedAllShippingRates();
        $costs = array();
        if ($rates) {
            foreach ($rates as $code => $rate) {
                if (!$helper->isCarrierCodeAllowed($code)) {
                    continue;
                }
                $carrier = array_shift($rate);
                $carrierCode = $carrier->getCarrier();
                $carrierTitle = $this->__($carrier->getCarrierTitle());
                $costInsurance = $helper->calculateInsuranceCost($carrierCode, $this->getQuote()->getSubtotal());
                $costs[$carrierTitle] = Mage::helper('core')->currency($costInsurance, true, false);
            }
            return $costs;
        }
        $carriers = Mage::getSingleton('shipping/config')->getActiveCarriers();
        foreach ($carriers as $carrierCode => $carrierModel) {
            if (!$helper->isCarrierCodeAllowed($carrierCode)) {
                continue;
            }
            $carrierTitle = $this->__(Mage::getStoreConfig('carriers/' . $carrierCode . '/title'));
            $costInsurance = $helper->calculateInsuranceCost($carrierCode, $this->getQuote()->getSubtotal());
            $costs[$carrierTitle] = Mage::helper('core')->currency($costInsurance, true, false);
        }
        return $costs;
    }
}
