<?php

class Itransition_ShippingInsurance_Model_Observer
{
    public function checkoutControllerOnepageSaveShippingMethod(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('itransition_shippinginsurance');
        $quote = $observer->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setInsuranceShippingMethod(null);
        $shippingAddress->setShippingInsurance(0);
        $quote->setInsuranceShippingMethod(null);
        $quote->setShippingInsurance(0);
        if (!$helper->isFeatureEnabled() || !$observer->getRequest()->getParam('shippinginsurance_enabled', false)) {
            return $this;
        }
        $carrierCode = null;
        foreach ( $shippingAddress->collectShippingRates()->getGroupedAllShippingRates() as $rate) {
            foreach ($rate as $carrier) {
                if ($shippingAddress->getShippingMethod() === $carrier->getCode()) {
                    $carrierCode = $carrier->getCarrier();
                }
            }
        }
        if (!$carrierCode) {
            return $this;
        }
        $allowed = $helper->isCarrierCodeAllowed($carrierCode);
        if (!$allowed) {
            return $this;
        }

        $cost = $helper->calculateInsuranceCost($carrierCode, $quote->getSubtotal());
        $shippingAddress->setInsuranceShippingMethod($carrierCode);
        $shippingAddress->setShippingInsurance($cost);
        $quote->setInsuranceShippingMethod($carrierCode);
        $quote->setShippingInsurance($cost);

        return $this;
    }
}
