# Shipping Insurance module for Magento 1    
Create module witch add shipping insurance to the order.  
![Screenshot from 2017-07-13 13-57-37.png](https://bitbucket.org/repo/7E9MXGk/images/1683120684-Screenshot%20from%202017-07-13%2013-57-37.png)   
You can switch on/off it and set cost value on settings. Cost of insurance can be constant or % from order for each shipping method.  
![Screenshot from 2017-07-13 13-59-45.png](https://bitbucket.org/repo/7E9MXGk/images/2805064041-Screenshot%20from%202017-07-13%2013-59-45.png)   
Add cost of shipping insurance to summery, order, chechout...  ![Screenshot from 2017-07-13 14-03-42.png](https://bitbucket.org/repo/7E9MXGk/images/1800449440-Screenshot%20from%202017-07-13%2014-03-42.png)